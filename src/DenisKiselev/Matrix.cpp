#include <time.h>
#include <fstream>
#include <string>
#include "Matrix.h"

Matrix::Matrix() : rows(0), cols(0), elements(nullptr) {};

Matrix::Matrix(int _rows, int _cols) : rows(_rows), cols(_cols)
{
	elements = new double*[rows];
	for (int i = 0; i < rows; i++)
	{
		elements[i] = new double[cols];
		for (int j = 0; j < cols; j++)
			elements[i][j] = 0.0;
	}
}

Matrix::Matrix(const Matrix& m) : rows(m.rows), cols(m.cols)
{
	elements = new double*[rows];
	for (int i = 0; i < rows; i++)
	{
		elements[i] = new double[cols];
		for (int j = 0; j < cols; j++)
			elements[i][j] = m.elements[i][j];
	}
}

Matrix::~Matrix()
{
		delete[] elements;
}

Matrix Matrix::operator+(const Matrix& m)
{
	if (rows != m.rows || cols != m.cols)
	{
		cout << "������. ������� ������ ��������." << endl;
		return Matrix();
	}
	Matrix tmp(rows, cols);
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			tmp.elements[i][j] = elements[i][j] + m.elements[i][j];
	return tmp;
}

Matrix Matrix::operator-(const Matrix& m)
{
	if (rows != m.rows || cols != m.cols)
	{
		cout << "������. ������� ������ ��������." << endl;
		return Matrix();
	}
	Matrix tmp(rows, cols);
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			tmp.elements[i][j] = elements[i][j] - m.elements[i][j];
	return tmp;
}

Matrix Matrix::operator*(const Matrix& m)
{
	if (cols != m.rows)
	{
		cout << "������. ����� �������� ������ ������� �� ����� ����� ����� ������." << endl;
		return Matrix();
	}
	Matrix tmp(rows, m.cols);
	for (int i = 0; i < tmp.rows; i++)
		for (int j = 0; j < tmp.cols; j++)
			for (int k = 0; k < cols; k++)
				tmp.elements[i][j] += elements[i][k] * m.elements[k][j];
	return tmp;
}

Matrix Matrix::operator*(double n)
{
	Matrix tmp(rows, cols);
	for (int i = 0; i < tmp.rows; i++)
		for (int j = 0; j < tmp.cols; j++)
			tmp.elements[i][j] = elements[i][j] * n;
	return tmp;
}

Matrix Matrix::operator/(double n)
{
	Matrix tmp(rows, cols);
	for (int i = 0; i < tmp.rows; i++)
		for (int j = 0; j < tmp.cols; j++)
			tmp.elements[i][j] = elements[i][j] / n;
	return tmp;
}

Matrix& Matrix::operator=(const Matrix& m)
{
	if (this != &m)
	{
		if (rows != m.rows || cols != m.cols)
		{
			delete [] elements;
			rows = m.rows;
			cols = m.cols;
			elements = new double*[rows];
			for (int i = 0; i < rows; i++)
				elements[i] = new double[cols];
		}
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
				elements[i][j] = m.elements[i][j];
	}
	return *this;
}

Matrix& Matrix::operator+=(const Matrix& m)
{
	if (rows != m.rows || cols != m.cols)
	{
		cout << "������. ������� ������ ��������." << endl;
		return Matrix();
	}
	Matrix tmp(rows, cols);
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			this->elements[i][j]  += m.elements[i][j];
	return *this;
}

Matrix& Matrix::operator-=(const Matrix& m)
{
	if (rows != m.rows || cols != m.cols)
	{
		cout << "������. ������� ������ ��������." << endl;
		return Matrix();
	}
	Matrix tmp(rows, cols);
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			this->elements[i][j]  -= m.elements[i][j];
	return *this;
}

Matrix& Matrix::operator*=(const Matrix& m)
{
	*this = *this * m;
	return *this;
}

Matrix& Matrix::operator*=(double n)
{
	for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
				this->elements[i][j] *= n;
	return *this;
}

Matrix& Matrix::operator/=(double n)
{
	for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
				this->elements[i][j] /= n;
	return *this;
}

bool Matrix::operator==(const Matrix& m) const
{
	if (rows == m.rows && cols == m.cols)
	{
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
				if (elements[i][j] != m.elements[i][j])
					return false;
	}
	else return false;
	return true;			
}

bool Matrix::operator!=(const Matrix& m) const
{
	if (rows == m.rows && cols == m.cols)
	{
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
				if (elements[i][j] != m.elements[i][j])
					return true;
	}
	else return true;
	return false;			
}

double*& Matrix::operator[](int index) const
{
	return elements[index];
}


double Matrix::determinant()
{
	double det = 0.0;
	if (rows != cols)
	{
		cout << "������. ������������ ����� ��������� ������ ��� ���������� �������." << endl;
		return 0;
	}
	if (rows == 1) return elements[0][0];
	for (int i = 0; i < cols; i++)
		if (i%2 == 0)	det += elements[0][i] * (this->minor(0, i).determinant());
		else det -= elements[0][i] * (this->minor(0, i).determinant());
	return det;
}

Matrix Matrix::minor(int iDop, int jDop)
{ 
	Matrix tmp(rows - 1, cols - 1);
	for (int i = 0; i < tmp.rows; i++)
		for (int j = 0; j < tmp.cols; j++)
		{
			if (i < iDop && j < jDop)	tmp.elements[i][j] = elements[i][j];
			if (i < iDop && j >= jDop)	tmp.elements[i][j] = elements[i][j + 1];
			if (i >= iDop && j < jDop)	tmp.elements[i][j] = elements[i + 1][j];
			if (i >= iDop && j >= jDop) tmp.elements[i][j] = elements[i + 1][j + 1];
		}
	return tmp;
}

Matrix Matrix::inverse()
{
	double det = determinant();
	if (det == 0)
	{
		cout << "������������ ������� ����� ����. �������� ������� �� ����������." << endl;
		return Matrix();
	}
	Matrix minor(rows, cols), transp;
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			if ((i + j) % 2 == 0)	minor.elements[i][j] = this->minor(i, j).determinant();
			else	minor.elements[i][j] = (-1) * (this->minor(i, j).determinant());
	transp = minor.transpose();
	return transp / det;
}

Matrix Matrix::transpose()
{
	Matrix tmp(cols, rows);
	for (int i = 0; i < tmp.rows; i++)
		for (int j = 0; j < tmp.cols; j++)
			tmp.elements[i][j] = elements[j][i];
	return tmp;
}

void Matrix::randomMatrix()
{
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			elements[i][j] = rand()%20 - 10;
}

void Matrix::readMatrix(string file)
{
	int rs = 0, cs = 0;
	char ch;
	string num;
	ifstream f(file);
	do
	{
		ch = f.get();
		if (ch == ' '&& rs == 0) cs++;
		if (ch == '\n') rs++;
	}
	while(!f.eof());
	if (rows != rs || cols != cs)
	{
		delete [] elements;
		rows = rs + 1;
		cols = cs + 1;
		elements = new double*[rows];
		for (int i = 0; i < rows; i++)
			elements[i] = new double[cols];
	}
	f.clear();
	f.seekg(0);
	rs = 0;
	cs = 0;
	while(rs != rows)
	{
		ch = f.get();
		if (ch == ' ')
		{
			elements[rs][cs++] = atof(num.c_str());
			num = "";
		}
		else if (ch == '\n' || f.eof())
		{
			elements[rs++][cs] = atof(num.c_str());
			num = "";
			cs = 0;
		}
		else num += ch;
	}
	f.close();
}

void Matrix::saveMatrix(string file)
{
	ofstream f(file);
	int det = determinant();
	f << "�������: " << endl;
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
			f << elements[i][j] << " ";
		f << endl;
	}
	if (det == 0) f << endl << "������������ � �������� ������� ���, �.�. ������� �� ����������." << endl << endl;
	else 
	{
		f << endl << "������������: " << det << endl << endl;
		f << "�������� �������: " << endl << inverse() << endl;
	}
	f << "����������������� �������: " << endl << transpose();
	f.close();
}

ostream& operator<<(ostream& stream, const Matrix& m)
{
	for (int i = 0; i < m.rows; i++)
	{
		for (int j = 0; j < m.cols; j++)
			stream << m.elements[i][j] << "  ";
		stream << endl;
	}
	return stream;
}