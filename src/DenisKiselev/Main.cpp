#include <string>
#include <time.h>
#include <clocale>
#include "Matrix.h"

using namespace std;

int main()
{
	setlocale(LC_CTYPE, "Russian");
	srand(time(NULL));
	Matrix a;
	a.readMatrix("Matrix.txt");
	a.saveMatrix("Result.txt");
	return 0;
}