﻿
#include <iostream>

using namespace std;

class Matrix 
{
private:
	int rows;
	int cols;
	double** elements;
public:
	Matrix();
	Matrix(int rows, int cols);
	Matrix(const Matrix& m);
	~Matrix();

	friend ostream& operator<<(ostream& stream, const Matrix& m);
	Matrix operator+(const Matrix& m);
	Matrix operator-(const Matrix& m);
	Matrix operator*(const Matrix& m);
	Matrix operator*(double n);
	Matrix operator/(double n);
	Matrix& operator=(const Matrix& m);
	Matrix& operator+=(const Matrix& m);
	Matrix& operator-=(const Matrix& m);
	Matrix& operator*=(const Matrix& m);
	Matrix& operator*=(double n);
	Matrix& operator/=(double n);
	bool operator==(const Matrix& m) const;
	bool operator!=(const Matrix& m) const;
	double*& operator[](int index) const;

	double determinant();
	Matrix minor(int iDop, int jDop);
	Matrix inverse();
	Matrix transpose();
	void randomMatrix();
	void readMatrix(string file);
	void saveMatrix(string file);
};

ostream& operator<<(ostream& stream, const Matrix& m);
